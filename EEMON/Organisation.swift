//
//  Organisation.swift
//  EEMON
//
//  Created by Jelle Braat on 04/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation

class Organisation{
    
    var uuID : Int
    var name : String
    var description: String
    var buildings: [Building]?
    
    init(uuid: Int, name: String, description: String){
        self.uuID = uuid
        self.name = name
        self.description = description
    }
    
    func setBuildings (buildings: [Building])
    {
        self.buildings = buildings
    }
}
