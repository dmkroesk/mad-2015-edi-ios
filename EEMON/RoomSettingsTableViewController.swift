//
//  RoomSettingsTableViewController.swift
//  EEMON
//
//  Created by Jelle Braat on 12/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class RoomSettingsTableViewController: UITableViewController, doneNTRoomTableViewController {

    var building : Building?
    var rooms : [Location] = [Location]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let buildingName = building?.name.capitalizedString
        {
            self.navigationItem.title = buildingName
        }
        else
        {
            self.navigationItem.title = "Rooms"
        }
        NetworkHandler.networkInstance.delegateNTRoomTableViewController = self
    }
    
    override func viewDidAppear(animated: Bool) {
        NetworkHandler.networkInstance.getRoomsForRoomTableViewController(building!)
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func roomsReceived (rooms: [Location])
    {
        self.rooms = rooms
        self.tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("roomCell")! as UITableViewCell
        
        cell.textLabel!.text = rooms[indexPath.row].name
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let room : Location = rooms[indexPath.row]

        let defaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        defaults.setObject(room.uuID, forKey: "DefaultRoomId")
        
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}
