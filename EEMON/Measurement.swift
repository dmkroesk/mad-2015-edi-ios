//
//  Measurement.swift
//  EEMON
//
//  Created by Pascal Huijsmans on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation

class Measurement: NSObject {
    
    var sensorID : Int
    var dateTime : NSDate
    var unit : String
    var value : Double
    
    init(sensorID: Int, timeStamp: NSDate, unit: String, value: Double )
    {
        self.sensorID = sensorID
        self.dateTime = timeStamp
        self.unit = unit
        self.value = value
    }
}