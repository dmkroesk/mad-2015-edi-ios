//
//  SensorHolder.swift
//  EEMON
//
//  Created by Jelle Braat on 20/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class DataHolder {
    static var StaticActiveRoom : Location?
    static var StaticImageView : UIImage?
    
    static var roomListViewController : RoomListViewController?
    static var singleChartViewController : SingleChartViewController?
    
//    Remove?
//    static var StaticSensorListSingleChart : [Sensor] = []
    
    // use like this in a for loop
    /* 
    for var index = 0; index < array.count; ++index {
        let colorIndex = index > 4 ? index % 5  : index
        property.background = DataHolder.StaticChartColors[colorIndex].CGColor
    } 
    */
    static var StaticChartColors : [UIColor] = [
        UIColor(red: (1/255) * 51, green: (1/255) * 77, blue: (1/255) * 92, alpha: 1), // Blue
        UIColor(red: (1/255) * 69, green: (1/255) * 178, blue: (1/255) * 157, alpha: 1), // Turquoise
        UIColor(red: (1/255) * 239, green: (1/255) * 201, blue: (1/255) * 76, alpha: 1), // Yellow
        UIColor(red: (1/255) * 226, green: (1/255) * 122, blue: (1/255) * 63, alpha: 1), // Orange
        UIColor(red: (1/255) * 223, green: (1/255) * 90, blue: (1/255) * 73, alpha: 1) // Red
    ]
    
    static var addSensorPopDelegate : AddSensorDelegate?
}

protocol AddSensorDelegate{
    func didAddSensorPop(sensor: Sensor)
}
