//
//  SingleChartViewController.swift
//  EEMON
//
//  Created by Jelle Braat on 19/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class SingleChartViewController: UIViewController, AddSensorDelegate{

    var sensors : [Sensor]? = []
    
    @IBOutlet weak var ChartPlotter: ChartPlotterView!
    @IBOutlet weak var NodeInfo: SingleChartNodeInfoUIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataHolder.addSensorPopDelegate = self
        DataHolder.singleChartViewController = self
        
//        if (sensors == nil || sensors?.count <= 0 || sensors?[0].measurements.count <= 0){
//            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
//            self.navigationController?.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
//            return
//        }
        
        ChartPlotter.isSingleChart = true
        if (sensors?.count <= 5){
            for sens in sensors!{
                ChartPlotter.sensors?.append(sens)
                NodeInfo.sensors?.append(sens)
            }
        }
        else {
            for var index = 0; index < sensors?.count; ++index{
                if (index >= 5){
                    break
                }
                ChartPlotter.sensors?.append(sensors![index])
                NodeInfo.sensors?.append(sensors![index])
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        print("viewDidAppear")
    }
    
    override func viewWillDisappear(animated: Bool) {
    }
    
    func nodeInfoAddButton(sender:UIButton!)
    {
        performSegueWithIdentifier("navigateToContentChartViewAddSensor", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "navigateToContentChartViewAddSensor" {
            if let destination = segue.destinationViewController as? ContentViewController {
                destination.isAddSensorToChart = true
            }
        }
    }
    
    func didAddSensorPop(sensor: Sensor){
        sensors?.append(sensor)
        // magic fix for chartplotter sensors
        ChartPlotter.sensors?.removeAll()
        ChartPlotter.sensors?.append(sensor)
        NodeInfo.sensors = sensors
        //Refresh views
        ChartPlotter.setNeedsDisplay()
        NodeInfo.setNeedsDisplay()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
