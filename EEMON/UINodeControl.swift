//
//  UINodeControl.swift
//  EEMON
//
//  Created by jack on 10/15/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class UINodeControl: UIButton {
    
    var heatColor = UIColor.greenColor()
    var dotColor = UIColor.blackColor()
    
    var sensor : Sensor?
    
    var watt : Double = 0
 
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        
        
        // Sensor appearence on map
        let alpha = CGFloat(0.7)
        var caseColor : [CGFloat]
        
        self.setDotColor(0, g: 0, b: 0, alpha: alpha)
        
        switch(watt)
        {
            
        case 0..<500:
            caseColor = [86, 185, 217]
            break;
        case 500..<1000:
            caseColor = [165, 218, 84]
            break;
        case 1000..<1500:
            caseColor = [247, 230, 28]
            break;
        case 1500..<2000:
            caseColor = [255, 171, 6]
            break;
        case 2000..<3000:
            caseColor = [208, 1, 66]
            break;
        default:
            caseColor = [200, 200, 200]
            break;
        }
        
        self.setHeatColor((1/255)*caseColor[0], g: (1/255)*caseColor[1], b: (1/255)*caseColor[2], alpha: alpha)
        self.setDotColor((1/255)*caseColor[0], g: (1/255)*caseColor[1], b: (1/255)*caseColor[2], alpha: 1)
        
        
        //nodeOutline
        let path = UIBezierPath(ovalInRect: rect)
        heatColor.setFill()
        path.fill()
        
        
        //center dot
        let newWidth = (rect.width / 4)
        let newHeight = (rect.height / 4)
        let resizedRect = CGRect(x: rect.midX - newWidth/2, y: rect.midY - newHeight/2, width: newWidth, height: newHeight)
        
        let centerDot = UIBezierPath(ovalInRect: resizedRect)
        
        
        
        dotColor.setFill()
        centerDot.fill()
    }
    
    func setPower(watt : Double)
    {
        self.watt = watt
    }
    
    func setHeatColor(r : CGFloat, g : CGFloat, b : CGFloat, alpha : CGFloat)
    {
        heatColor = UIColor(red: r, green: g, blue: b, alpha: alpha)
        self.setNeedsDisplay() //redraw
    }
    
    func setDotColor(r : CGFloat, g : CGFloat, b : CGFloat, alpha : CGFloat)
    {
        dotColor = UIColor(red: r, green: g, blue: b, alpha: alpha)
        self.setNeedsDisplay() //redraw
    }

}
