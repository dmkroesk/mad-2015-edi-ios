//
//  BuildingTableViewController.swift
//  EEMON
//
//  Created by Jelle Braat on 12/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class BuildingTableViewController: UITableViewController, doneNTBuildingTableViewController {

    var organisation : Organisation?
    var buildings: [Building] = [Building]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let organisationName = organisation?.name.capitalizedString
        {
            self.navigationItem.title = organisationName
        }
        else
        {
            self.navigationItem.title = "Buildings"
        }
        
        NetworkHandler.networkInstance.delegateNTBuildingTableViewController = self
    }
    
    override func viewDidAppear(animated: Bool) {
        NetworkHandler.networkInstance.getBuildingsForBuildingTableViewController(organisation!)
    }

    func buildingsReceived (buildings: [Building])
    {
        self.buildings = buildings
        self.tableView.reloadData()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buildings.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("buildingCell")! as UITableViewCell
        
        cell.textLabel!.text = buildings[indexPath.row].name
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "navigateToRoomSettings" {
            if let destination = segue.destinationViewController as? RoomSettingsTableViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow{
                    
                    let building: Building = buildings[indexPath.row]
                    
                    let defaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(building.uuID, forKey: "DefaultBuildingId")
                    
                    destination.building = building
                }
            }
        }
    }
}
