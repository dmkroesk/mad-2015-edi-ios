//
//  ChartPlotterHandler.swift
//  EEMON
//
//  Created by Jelle Braat on 13/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation
import UIKit

class ChartPlotterView : UIView {
    
    var sensors : [Sensor]? = []
    var graphPoints : [[CGFloat]] = []
    var isSingleChart : Bool = false
    var spacingLeft = CGFloat(116)
    var spacingRight = CGFloat(40)
    var firstDate = NSDate?()
    var lastDate = NSDate?()
    
    var maxValueY : Double = Double(0.0)
    
    //The rectangle given to this function, the chart will be plotted on top of it
    override func drawRect(rect: CGRect) {
        if (nil != sensors && sensors!.count > 0){
            fillPointsWithSensor(sensors!)
        }
        
        var x = CGFloat(0)
        var realRect = rect
        var newHeight = rect.height
        if (isSingleChart){
            x = spacingLeft
            newHeight = rect.height - spacingLeft
            realRect = CGRect(x: x, y: 0, width: rect.width, height: newHeight)
        } else {
            spacingLeft = 0
            spacingRight = 0
        }
        
        let backgroundLayers = drawBackground(realRect, x: x)
        let axis = drawAxis(realRect, x: x)
        
        axis.strokeColor = UIColor.blackColor().CGColor
        axis.lineWidth = 2.0
        
        // achtergrond tekenen
        for bgLayer in backgroundLayers{
            self.layer.addSublayer(bgLayer)
        }

        // axis tekeken
        self.layer.addSublayer(axis)
        
        // schaalverdeling tekenen (zolang er geen collection view is)
        if (isSingleChart){
            let scale = drawScaleVertical(realRect)
            let scaleHor = drawScaleHorizontal(realRect)
            self.layer.addSublayer(scale)
            self.layer.addSublayer(scaleHor)
        }
        for var index = 0; index < graphPoints.count; ++index{
            // grafiek tekenen
            let colorIndex = index > 4 ? index % 5  : index

            let drawGraph = graphLineLayer(x, rect: realRect, drawGraphPoints: graphPoints[index])
            drawGraph.strokeColor = DataHolder.StaticChartColors[colorIndex].CGColor
            drawGraph.fillColor = UIColor.clearColor().CGColor
            drawGraph.lineWidth = 2.0
            self.layer.addSublayer(drawGraph)
        }
    }
    
    func fillPointsWithSensor(sensors: [Sensor]){
        //voor elke sensor in de sensorlijst haal de measurements op
        for var index = 0; index < sensors.count; ++index {
            let sensor = sensors[index]
            
            var graphPointsSensor : [CGFloat] = []
            //elke meting in de graphpoint array zetten
            for var ind = 0; ind < sensor.measurements.count; ++ind {
                let measurement = sensor.measurements[ind]
                graphPointsSensor.append(CGFloat(measurement.value))
                
                if(measurement.value > self.maxValueY)
                {
                    self.maxValueY = measurement.value + (100 - (measurement.value % 100))
                }
            }
            //todo make if for multiple sensors
            firstDate = sensor.measurements.first?.dateTime
            lastDate = sensor.measurements.last?.dateTime
            //voeg de lijst measurements toe aan de lijst van graphpoints
            graphPoints.append(graphPointsSensor)
        }
    }
   
    func drawBackground(rect: CGRect, x: CGFloat) -> [CAShapeLayer]{
        //TODO: Maak lijnen breder en smaller
        let countStripes = 8
        let width = rect.width - spacingRight
        let widthPart = width / CGFloat(countStripes)
        var layers : [CAShapeLayer] = []
        let layerFrame = CGRect(x: x, y: 0, width: widthPart, height: rect.height)
        
        for var index = 0; index < countStripes; ++index {
            let SuperLayer = CAShapeLayer()
            
            SuperLayer.frame = layerFrame
            
            if (index % 2 == 1){
                SuperLayer.backgroundColor = UIColor(red: (1/255) * 242, green: (1/255) * 242, blue: (1/255) * 242, alpha: 1.0).CGColor
            }else{
                SuperLayer.backgroundColor = UIColor.whiteColor().CGColor
            }
            //Magic?
            let posX = Int(widthPart) * index + Int(widthPart/2)
            SuperLayer.position = CGPointMake(CGFloat(posX), rect.height/2)
            
            layers.append(SuperLayer)
        }
        return layers
    }
    
    func graphLineLayer(x: CGFloat, rect: CGRect, drawGraphPoints: [CGFloat]) -> CAShapeLayer
    {
        let graph = CAShapeLayer()
        
        let ymax = rect.height
        let maxValue = CGFloat(self.maxValueY)
        
        // Normaliseer de data uit serie naar de grafiek oppervlakte
        let scaledY = drawGraphPoints.map { $0 * ymax / maxValue }
        
        // Teken de grafiek
        let path = UIBezierPath()
        path.moveToPoint(CGPoint(x:x, y:ymax - scaledY[0]))
        
        for (index, y) in scaledY.enumerate() {
            let x = CGFloat(index) * (rect.width - (spacingLeft + spacingRight)) / CGFloat(drawGraphPoints.count) + spacingLeft
            path.addLineToPoint(CGPoint(x:x, y:(ymax - y) - 5))
        }
        
        graph.path = path.CGPath
        
        return graph
    }
    
    //Deprecated?
    func graphDotLayer(rect: CGRect, drawGraphPoints: [CGFloat]) -> CAShapeLayer
    {
        let graph = CAShapeLayer()
        
        let maxHeight = rect.height
        let maxValue = drawGraphPoints.maxElement()!
        let dotRadius : CGFloat = 5.0
        
        // Normaliseer de data uit serie naar de grafiek oppervlakte
        let scaledY = drawGraphPoints.map { $0 * maxHeight / maxValue }
        
        for (index, y) in scaledY.enumerate() {
            let dotLayer = CAShapeLayer()
            let x = 10.0 + CGFloat(index) * (rect.width - 20.0) / CGFloat(drawGraphPoints.count)
            let y = maxHeight + 10.0 - y
            var point = CGPoint(x:x, y:y)
            point.x -= dotRadius/2
            point.y -= dotRadius/2
            
            let circle = UIBezierPath(ovalInRect: CGRect(origin: point,
                size: CGSize(width: dotRadius, height: dotRadius)))
            dotLayer.path = circle.CGPath
            dotLayer.strokeColor = UIColor.clearColor().CGColor
            dotLayer.fillColor = UIColor.whiteColor().CGColor
            graph.addSublayer(dotLayer)
        }
        return graph
    }
    
    func drawAxis(rect: CGRect, x: CGFloat) -> CAShapeLayer{
        let axis = CAShapeLayer()
        let path = UIBezierPath()
 
        // Draw x Axis
        let p0 = CGPointMake(x, rect.height)
        let p1 = CGPointMake(rect.width - spacingRight, rect.height)
        path.moveToPoint(p0)
        path.addLineToPoint(p1)
        // Draw y Axis
        let p2 = CGPointMake(x, 0)
        let p3 = CGPointMake(x, rect.height)
        path.moveToPoint(p2)
        path.addLineToPoint(p3)
        
        // Draw other x
        let p4 = CGPointMake(x, 0)
        let p5 = CGPointMake(rect.width - spacingRight, 0)
        path.moveToPoint(p4)
        path.addLineToPoint(p5)
        
        // Draw other y
        let p6 = CGPointMake(rect.width - spacingRight, 0)
        let p7 = CGPointMake(rect.width - spacingRight, rect.height)
        path.moveToPoint(p6)
        path.addLineToPoint(p7)
        
        //Add path to axis
        axis.path = path.CGPath
        
        return axis
    }
    
    func drawScaleVertical(rect: CGRect) -> CAShapeLayer
    {
        let scale = CAShapeLayer()
        
        let labels = 8
        var i = 0
        
        for(; i < labels + 1; i++)
        {
            let label = CATextLayer()
            label.fontSize = CGFloat(16)
            label.alignmentMode = kCAAlignmentRight
            label.font = "System"
            label.string = Int(self.maxValueY - ((self.maxValueY / Double(labels)) * Double(i))).description + "W"
            label.foregroundColor = UIColor.blackColor().CGColor
            label.frame = CGRect(x: 0, y: -10 + ((rect.height / CGFloat(labels)) * CGFloat(i)), width: self.spacingLeft - 5,height: 200)
            
            scale.addSublayer(label)
        }
        
        return scale
    }
    
    func drawScaleHorizontal(rect: CGRect) -> CAShapeLayer
    {
        let scale = CAShapeLayer()
        //labels is the ammount of labels to the beginning and end
        let labels = 5
        //divide the ammount of sensor values by 5
        let valuesfordate = (sensors![0].measurements.count - 1) / 4
        
        let labelFrame = CGRect(x: 0, y: 0, width: 50, height: 64)
        let startX = rect.minX - (labelFrame.width/2)
        let endX = rect.width - labelFrame.width/2 - self.spacingRight
        let labelSpace = (endX - startX) / CGFloat(labels - 1)

        for var i = 0; i < labels; ++i {
            //TODO: fix the empty array (sometimes a fatel error: Array index out of range)
            if let meas = sensors?[0].measurements[valuesfordate * (i)]{
                let label = makeDateLabel(meas.dateTime)
                label.frame.origin = CGPointMake(startX + (labelSpace * CGFloat(i)), rect.height + 10)
                scale.addSublayer(label)
            }
        }
        return scale
    }
    
    func makeDateLabel(date: NSDate) -> CAShapeLayer
    {
        let labelLayer = CAShapeLayer()
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd MMM"
        let upDateString = formatter.stringFromDate(date)
        
        let upLabel = makeDateLabelComponent(upDateString)
        upLabel.alignmentMode = kCAAlignmentCenter
        upLabel.frame = CGRect(x: 0, y: 0, width: 50, height: 32)
        
        formatter.dateFormat = "HH:mm"
        let downDateString = formatter.stringFromDate(date)
        let downLabel = makeDateLabelComponent(downDateString)
        downLabel.alignmentMode = kCAAlignmentCenter
        downLabel.frame = CGRect(x: 0, y: 20, width: 50, height: 32)

        labelLayer.addSublayer(upLabel)
        labelLayer.addSublayer(downLabel)
        
        return labelLayer
    }
    
    func makeDateLabelComponent(dateString: String) -> CATextLayer {
        let label = CATextLayer()
        label.fontSize = CGFloat(17)
        label.alignmentMode = kCAAlignmentRight
        label.font = "System"
        label.foregroundColor = UIColor.blackColor().CGColor
        
        label.string = dateString
        return label
    }
}
