//
//  SettingsOverlayViewController.swift
//  EEMON
//
//  Created by Jack Evers on 22/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class SettingsOverlayViewController: UIViewController {

    @IBAction func defaultLocationsBtn(sender: AnyObject) {
        performSegueWithIdentifier("navigateToDefaultSettings", sender: self)
    }
    
    @IBAction func notificationsBtn(sender: AnyObject) {
        performSegueWithIdentifier("navigateToNotifications", sender: self)
    }
    
    @IBAction func aboutBtn(sender: AnyObject) {
        performSegueWithIdentifier("navigateToAbout", sender: self)
    }
    
    @IBOutlet weak var settingsDialog: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        UIHelper.applyPlainShadow(settingsDialog)
        UIHelper.applyCornerRadius(settingsDialog, topView: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
