//
//  ChartCollectionViewCell.swift
//  EEMON
//
//  Created by Jelle Braat on 20/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class ChartCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ChartView: ChartPlotterView!
    @IBOutlet weak var SensorNameButton: UIButton!
    var chartSensor : Sensor?
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        print(ChartView.frame)
        
        self.layer.addSublayer(drawBorder(rect))
    }
    
    func drawBorder(rect: CGRect) -> CAShapeLayer{
        let axis = CAShapeLayer()
        let path = UIBezierPath()
        
        // Draw x Axis
        let p0 = CGPointMake(0, rect.height)
        let p1 = CGPointMake(rect.width, rect.height)
        path.moveToPoint(p0)
        path.addLineToPoint(p1)
        // Draw y Axis
        let p2 = CGPointMake(0, 0)
        let p3 = CGPointMake(0, rect.height)
        path.moveToPoint(p2)
        path.addLineToPoint(p3)
        
        // Draw other x
        let p4 = CGPointMake(0, 0)
        let p5 = CGPointMake(rect.width, 0)
        path.moveToPoint(p4)
        path.addLineToPoint(p5)
        
        // Draw other y
        let p6 = CGPointMake(rect.width, 0)
        let p7 = CGPointMake(rect.width, rect.height)
        path.moveToPoint(p6)
        path.addLineToPoint(p7)
        
        //Add path to axis
        axis.path = path.CGPath
        
        return axis
    }

}
