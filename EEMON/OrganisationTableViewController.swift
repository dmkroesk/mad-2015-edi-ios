//
//  OrganisationTableViewController.swift
//  EEMON
//
//  Created by Jelle Braat on 12/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class OrganisationTableViewController: UITableViewController, doneNTOrganisationTableViewController {

    var organisations: [Organisation] = [Organisation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Organisation";
        
        NetworkHandler.networkInstance.delegateNTOrganisationTableViewController = self
    }
    
    override func viewWillAppear(animated: Bool) {
        
        NetworkHandler.networkInstance.getOrganisationsForOrganisationTableViewController()
    }

    func organisationsReceived (organisations: [Organisation])
    {
        self.organisations = organisations
        self.tableView.reloadData()
    }

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return organisations.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("organisationCell")! as UITableViewCell
        
        cell.textLabel!.text = organisations[indexPath.row].name
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "navigateToBuildingSettings" {
            if let destination = segue.destinationViewController as? BuildingTableViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow{
                    let organisation: Organisation = organisations[indexPath.row]
                    
                    let defaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(organisation.uuID, forKey: "DefaultOrganisationId")
                    
                    destination.organisation = organisation
                }
            }
        }
    }
}
