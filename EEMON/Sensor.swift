//
//  Sensor.swift
//  EEMON
//
//  Created by Jelle Braat on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation
import UIKit
import Darwin

class Sensor: NSObject {
    
    var uuID : Int
    var name : String
    
    var nodeID : Int
    //var timestamp : datum?
    
    var deviceIdentifier : String
    var type : String
    var descriptionSensor : String
    var mapLocationX : Float
    var mapLocationY : Float
    
    var heatDiameter : CGFloat
    
    var measurements : [Measurement]
    
    var actionTarget : UIViewController?
    
    var location : Location
    

    init(uuid: Int, name: String, nodeID: Int, deviceIdentifier: String, type: String, descriptionSensor: String, mapLocationX: Float, mapLocationY: Float, location : Location)
    {
        self.uuID = uuid
        self.name = name
        self.nodeID = nodeID
        self.deviceIdentifier = deviceIdentifier
        self.type = type
        self.descriptionSensor = descriptionSensor
        self.mapLocationX = mapLocationX
        self.mapLocationY = mapLocationY
        self.measurements = [Measurement]()
        self.location = location
        
        self.heatDiameter = CGFloat(100) //TODO heatDiameter opnemen in centrale plek voor app settings
    }
    
    func setActionTrgt(actionTarget : UIViewController)
    {
        self.actionTarget = actionTarget
    }
    
    func draw(nodeCanvas: UIView, onDateTime: NSDate)
    {
        let scaleX = Float(nodeCanvas.bounds.width) / self.location.width
        let scaleY = Float(nodeCanvas.bounds.height) / self.location.height
        let x = CGFloat(mapLocationX * scaleX)
        let y = CGFloat(mapLocationY * scaleY)
        
        let testFrame : CGRect = CGRectMake(x,y,heatDiameter,heatDiameter)
        let nodeView : UINodeControl = UINodeControl(frame: testFrame)
        //UINodeControl drawRect() func is being called when nodeView is being drawn.
        
        nodeView.addTarget(actionTarget, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        nodeView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.0) //white background with 0 alpha
        nodeView.alpha = 1 // alpha of 0.5 for the content on this view.
        
        
        nodeView.setPower( getMeasurementClosestTo(onDateTime) )
        
        nodeView.accessibilityIdentifier = "sensorView"
        nodeView.sensor = self
        
        nodeCanvas.addSubview(nodeView)
        
        
        //nodeView.setNeedsDisplay()
    }
    
    func draw(watt: Int, nodeCanvas: UIView)
    {
        draw(nodeCanvas, onDateTime: NSDate())
    }
    
    func getMeasurementClosestTo(onDateTime: NSDate) -> Double
    {
        var lowerIndex = 0
        var upperIndex = measurements.count - 1
        
        if (measurements.count <= 0)
        {
            return -1.0
        }
        
        let deltaTime = (3600.0/6) / 100 //delta is now 6 seconds. with measurements every 10 seconds this means there is a small overlap
        
        while(true)
        {
            let currentIndex = (lowerIndex + upperIndex) / 2
            if(measurements[currentIndex].dateTime.timeIntervalSince1970 < onDateTime.timeIntervalSince1970 + deltaTime &&
               measurements[currentIndex].dateTime.timeIntervalSince1970 > onDateTime.timeIntervalSince1970 - deltaTime )
            {
                print("currenntmeasurement DateTime: \(measurements[currentIndex].dateTime.description) : requestedDateTime: \(onDateTime.description) delta:\(deltaTime)")
                print("LowerandUpperIndex: \(lowerIndex):\(upperIndex)")
                return measurements[currentIndex].value
            }
            else if (lowerIndex > upperIndex)
            {
                print("got into else if: requestedate and lowerIndex.date and lowerIndex:upperIndex")
                print("\(onDateTime.description) lowerIndexDate: \(measurements[currentIndex].dateTime.description), lowerandUpperIndex: \(lowerIndex):\(upperIndex)")
                return -1.0
            }
            else
            {
                if(measurements[currentIndex].dateTime.timeIntervalSince1970 > onDateTime.timeIntervalSince1970)
                {
                    upperIndex = currentIndex - 1
                }
                else
                {
                    lowerIndex = currentIndex + 1
                }
            }
        }
        
    }
    
    func getFullName() -> String
    {
        return self.name
    }
}
