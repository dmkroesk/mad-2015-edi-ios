//
//  UIHelper.swift
//  EEMON
//
//  Created by Willem van Strien on 27/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit
import Foundation

class UIHelper{
    
    static var CORNER_RADIUS = CGFloat(5.0)
    
    static func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = CORNER_RADIUS
    }
    
    static func applyCornerRadius(view: UIView, topView: Bool){
        if(topView == false){
         view.layer.cornerRadius = CORNER_RADIUS
        }
        
        else{
            view.layer.cornerRadius = CORNER_RADIUS
            
            let frame = CGRectMake(view.bounds.origin.x, view.bounds.origin.y, view.bounds.width, CORNER_RADIUS)
            
            print("cornerRadiusmask bounds: \(frame.width)")
            
            let cornerMask = UIView(frame: frame)
            cornerMask.backgroundColor = UIColor.whiteColor()
            
            view.addSubview(cornerMask)
        }

    }
    
    static func addLabelToView(view: UIView, text : String, point : CGPoint){
        let label = UILabel(frame: CGRect(x: view.bounds.origin.x - view.bounds.width, y: view.bounds.origin.y, width: view.bounds.width * 4, height: view.bounds.height))
        label.center = point
        label.textAlignment = NSTextAlignment.Left
        label.text = text
        label.font = UIFont.boldSystemFontOfSize(18.0)
        label.textColor = UIColor.whiteColor()
        view.addSubview(label)
    }
    
    func runAfterDelay(delay: NSTimeInterval, block: dispatch_block_t) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), block)
    }
    
    static func displayBackground(view: UIView, view viewController: UIViewController, action: Selector)
    {
        let frame : CGRect = CGRectMake(0,0, view.bounds.width, view.bounds.height)
        let v : UIView = UIView(frame: frame)
        
        v.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.7)
        v.accessibilityIdentifier = "background"
        
        // First: set the overlays alpha to 0
        v.alpha = 0
        
        // Second: animate the overlays alpha to 1
        UIView.animateWithDuration(0.1, animations: {
            v.alpha = 1.0
        })
        
        let tap = UITapGestureRecognizer(target: viewController, action: action)
        tap.delegate = viewController as? UIGestureRecognizerDelegate
        v.addGestureRecognizer(tap)
        
        view.addSubview(v)
    }
}
