//
//  Building.swift
//  EEMON
//
//  Created by Jelle Braat on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation

class Building {
    
    var uuID : Int
    var name : String
    var street : String
    var city : String
    var streetNumber : String
    var postalCode: String
    var descriptionBuilding : String
    var locations : [Location]?
    
    init (uuid: Int, name: String, street: String, city: String, streetNumber: String, postalCode: String, descriptionBuilding: String )
    {
        self.uuID = uuid
        self.name = name
        self.street = street
        self.city = city
        self.postalCode = postalCode
        self.streetNumber = streetNumber
        self.descriptionBuilding = descriptionBuilding
       
    }
    
    func setLocations (locations : [Location])
    {
         self.locations = locations
    }
}