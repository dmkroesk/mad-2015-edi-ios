//
//  Location.swift
//  EEMON
//
//  Created by Jelle Braat on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation

class Location {
    
    var uuID : Int
    var buildingID: Int
    var name : String
    var mapPath : String
    var width : Float
    var height : Float
    var descriptionLocation : String
    var sensors : [Sensor]?
    
    init(uuid: Int, name: String, mapPath: String, width: Float, height: Float, descriptionLocation: String, buildingID: Int)
    {
        self.uuID = uuid
        self.buildingID = buildingID
        self.name = name
        self.mapPath = mapPath
        self.width = width
        self.height = height
        self.descriptionLocation = descriptionLocation
        self.sensors = [Sensor]()
    }
}