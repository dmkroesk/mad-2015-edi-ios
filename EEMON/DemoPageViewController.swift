//
//  DemoPageViewController.swift
//  EEMON
//
//  Created by Pascal Huijsmans on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class DemoPageViewController: UIPageViewController, UIPageViewControllerDataSource {
   
    private let vcIdentiefiers : NSArray = ["DemoViewController_A","DemoViewController_B","DemoViewController_C"]
    
    // De ViewController die als eerste wordt getoond bij opstart.
    private let vcStartupIdentifier = "DemoViewController_A"
    
    // Continuous looping of stoppen aan begin/eind
    private var looping : Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        
        // Load from storyboard startup ViewController and set
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(vcStartupIdentifier)
        
        setViewControllers([vc], direction: .Forward, animated: false, completion: nil)
        
        // Set pagecontrol (bijna) doorzichtig
        UIPageControl.appearance().backgroundColor = UIColor.clearColor().colorWithAlphaComponent(0)
    }

    // Verander de view order om de pagecontrol bolletjes achtergrond transparant te maken
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //
        var scrollView : UIScrollView? = nil
        var pageControl: UIPageControl? = nil
        
        for view in self.view.subviews {
            if view.isKindOfClass(UIPageControl) { //   isKindOfClass:[UIPageControl class]]) {
                pageControl = view as? UIPageControl
            }
            if view.isKindOfClass(UIScrollView) { //   isKindOfClass:[UIPageControl class]]) {
                scrollView = view as? UIScrollView
            }
        }
        
        if( scrollView != nil && pageControl != nil )
        {
            scrollView?.frame = view.bounds
            view.bringSubviewToFront(pageControl!)
        }
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return vcIdentiefiers.count
    }
    
    //
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        print(pageViewController.description)
        return vcIdentiefiers.indexOfObject(vcStartupIdentifier)
    }
    
    //
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        if let identifier = viewController.restorationIdentifier
        {
            let index = vcIdentiefiers.indexOfObject(identifier)
            if index == 0 {
                if self.looping != true {
                    return nil
                }else{
                    return self.viewControllerNameAtIndex(self.vcIdentiefiers.count - 1)
                }
            }
            return viewControllerNameAtIndex(index-1)
        }
        return nil
    }
    
    //
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if let identifier = viewController.restorationIdentifier{
            let index = vcIdentiefiers.indexOfObject(identifier)
            if index == vcIdentiefiers.count - 1 {
                if self.looping != true {
                    return nil
                }else{
                    return self.viewControllerNameAtIndex(0)
                }
            }
            return viewControllerNameAtIndex(index+1)
        }
        return nil
    }
    
    // MARK: - Helper functies
    func viewControllerNameAtIndex(index: Int) -> UIViewController?
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let name = vcIdentiefiers.objectAtIndex(index) as? String
        {
            let vc = storyboard.instantiateViewControllerWithIdentifier(name)
            return vc
        }
        return .None
    }
}
