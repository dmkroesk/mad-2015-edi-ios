//
//  ChartCollectionViewController.swift
//  EEMON
//
//  Created by Jack Evers on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ChartCollectionViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes
        self.collectionView!.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return (DataHolder.StaticActiveRoom?.sensors!.count)!
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ChartCollectionCell", forIndexPath: indexPath) as! ChartCollectionViewCell
    
        // Configure the cell
        let sensor = DataHolder.StaticActiveRoom?.sensors![indexPath.row]
        
        cell.chartSensor = sensor
        //Button een object mee geven zodat hij weet naar welke ding hij toe moet gaan
        cell.SensorNameButton.setTitle(sensor?.getFullName(), forState: UIControlState.Normal)
        cell.ChartView.sensors?.append(sensor!)
        
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "navigateToSingleChartViewFromCollection" {
            if let destination = segue.destinationViewController as? SingleChartViewController {
                if let indexPath = self.collectionView?.indexPathForCell(sender as! ChartCollectionViewCell){
                    let sensorCell = DataHolder.StaticActiveRoom?.sensors![indexPath.row]
                    
                    destination.sensors?.append(sensorCell!)
                }
            }
        }

    }
    
    

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
