//
//  SingleChartNodeInfoUIView.swift
//  EEMON
//
//  Created by Jelle Braat on 03/12/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class SingleChartNodeInfoUIView: UIView {

    var sensors : [Sensor]? = []
    
    let width = CGFloat(186)
    var totalWidth : CGFloat = 0.0
    
    override func drawRect(rect: CGRect) {
        // Drawing code
        totalWidth = 0.0
        self.layer.sublayers?.removeAll()
        self.layer.addSublayer(makeNodeInfo(rect))
        
        for view in self.subviews{
            view.removeFromSuperview()
        }
        if (sensors?.count < 5){
            self.addSubview(makeAddNodeButton())
        } else {
            self.addSubview(makeOverlayView())
        }
    }
   
    func makeNodeInfo(rect: CGRect) -> CAShapeLayer {
        let nodeInfoLayer = CAShapeLayer()
        
        for var index = 0; index < sensors!.count; ++index{
            print(sensors?.count)
            let colorIndex = index > 4 ? index % 5  : index
            let layerFrame = CGRect(x: width*CGFloat(index)/2, y: 0, width: width, height: rect.height - 10)
            
            let nodeInfo = CAShapeLayer()
            nodeInfo.frame = layerFrame

            let color = CAShapeLayer()
            color.backgroundColor = DataHolder.StaticChartColors[colorIndex].CGColor
            color.cornerRadius = 8.0
            color.frame = CGRect(x: layerFrame.origin.x, y: 14, width: layerFrame.width/5, height: layerFrame.height/2 - 4)
            
            let label = CATextLayer()   
            label.fontSize = CGFloat(17)
            label.font = "System"
            label.string = sensors![index].getFullName()
            label.foregroundColor = UIColor.blackColor().CGColor
            label.frame = CGRect(x: layerFrame.origin.x + layerFrame.width/5 + 10, y: 10, width: layerFrame.width/1.5, height: layerFrame.height)
    
            totalWidth += width
            
            nodeInfo.addSublayer(color)
            nodeInfo.addSublayer(label)
            
            nodeInfoLayer.addSublayer(nodeInfo)
        }
        
        return nodeInfoLayer
    }
    
    func makeAddNodeButton() -> UIView{
        let frame = CGRect (x: totalWidth, y: 0, width: 44, height: 44)
        let addSensorView = UIView(frame: frame)

        let button = UIButton(type: UIButtonType.Custom) as UIButton
        button.frame = CGRect(x: 0, y: 0, width: 44,height: 44)
        if let image = UIImage(named: "Plus"){
            button.setImage(image, forState: .Normal)
        }
        if(nil != DataHolder.singleChartViewController)
        {
            button.addTarget(DataHolder.singleChartViewController, action: "nodeInfoAddButton:", forControlEvents: UIControlEvents.TouchUpInside)
        }
        
        let label = UILabel()
        label.frame = CGRect(x: 48, y: 0, width: 108, height: 44)
        label.font = UIFont(name: "System", size: 17)
        label.text = "Add Sensor"
        
        addSensorView.addSubview(label)
        addSensorView.addSubview(button)
        return addSensorView
    }
    
    func makeOverlayView() -> UIView{
        let frame = CGRect(x:totalWidth, y:0, width: 44, height: 44)
        let overlayHideButtonView = UIView(frame:frame)     
        return overlayHideButtonView
    }

    
}