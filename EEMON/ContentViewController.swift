//
//  ContentViewController.swift
//  EEMON
//
//  Created by jack on 10/8/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit


class ContentViewController: UIViewController, UIGestureRecognizerDelegate, doneNTContentViewController, RoomSelectedDelegate{

    @IBOutlet weak var heatmapImageView: UIImageView!
    @IBOutlet weak var heatmapImageOverlay: UIView!
    @IBOutlet weak var heatmapVignetteOverlay: UIImageView!
    @IBOutlet weak var nodeCanvas: UIView!
    
    @IBOutlet weak var overlayCanvas: UIView!
    
    @IBOutlet weak var roomBtnOutlet: UIButton!
    
    // ### Timelapse Control elements ###
    @IBOutlet weak var timelapseControlsView: UIView!
    //timeLapseControlsView members
    @IBOutlet weak var playPauzeBtnOutlet: UIButton!
    @IBOutlet weak var timeSliderOutlet: UISlider!
    
    @IBOutlet weak var timeIndicatorLabelOutlet: UILabel!
    
    // Activity indicator
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    

    let playImage = UIImage(named: "Play")
    let pauseImage = UIImage(named: "Pause")
    var isAddSensorToChart = false
    
    var dateTimeOnCreation = NSDate()

    @IBAction func playPauzeBtn(sender: AnyObject) {
        //actual action
        //if value is at 0 (value slider goes from -1 to 0. zo 0 is the value on the outer right side of the slider.. this corresponds with dateTime Now) set it to -1. (2 days ago)
        if(timeSliderOutlet.value == 0)
        {
            timeSliderOutlet.value = -1
        }
        print("play pauze button pressed")
        
        //appearence of the button
        if (playPauzeBtnOutlet.imageForState(.Normal) == playImage) {
            playPauzeBtnOutlet.setImage(pauseImage, forState: .Normal)
        } else {
            playPauzeBtnOutlet.setImage(playImage, forState: .Normal)
        }
        
        updateSelectedTime()
    }
    
    @IBAction func timeSliderValueChanged(sender: AnyObject) {
            changeTimeSliderValue()
        }
    
    func changeTimeSliderValue(){
        timeIndicatorLabelOutlet.adjustsFontSizeToFitWidth = false
        
        let secondsPerHour = Float(3600)
        let hoursInDay = Float(24)
        
        let startTimeInSec = dateTimeOnCreation.timeIntervalSince1970 - 10
        let dateMod = Double(timeSliderOutlet.value * 2 * hoursInDay * secondsPerHour)
        
        let selectedDate = NSDate(timeIntervalSince1970: startTimeInSec + dateMod)
        
        sensorViewControllerInstance?.dateToDisplay =  selectedDate
        
        setTimeIndicatorLabel(selectedDate)
        
        sensorViewControllerInstance?.drawSensorMeasurements()
        
        setMapImageOverlay()
    }
    
    func setMapImageOverlay()
    {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH"
        let hourString = formatter.stringFromDate((sensorViewControllerInstance?.dateToDisplay)!)
        var hour = Double(hourString)!
        
        formatter.dateFormat = "mm"
        let minuteString = formatter.stringFromDate((sensorViewControllerInstance?.dateToDisplay)!)
        let minutesInDecimalHours = Double(minuteString)! / 60
        
        hour += minutesInDecimalHours
        
        var deltaTime = abs(hour - 12)
        
        if(deltaTime > 5)
        {
            deltaTime -= 5;
        }
        else
        {
            deltaTime = 0
        }
        
        let alpha = CGFloat(0.05 * Double(deltaTime))
        //heatmapImageOverlay.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: alpha)
        heatmapVignetteOverlay.alpha = alpha
    }
    
    func setTimeIndicatorLabel(date: NSDate)
    {
        //Date formatter to for updating the label next to the slider
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd-MM-yy HH:mm"
        let dateString = formatter.stringFromDate(date)
        //setting the label on the button next to the slider
        timeIndicatorLabelOutlet.text = dateString
    }
    
        @IBAction func timeIndicatorBtn(sender: AnyObject) {
            
        }
    // ### Timelapse Control elements ###
    
    @IBOutlet weak var legendaView: UIView!
    
    @IBOutlet weak var chartBtnOutlet: UIButton!
    @IBOutlet weak var timelapseBtnOutlet: UIButton!

    @IBAction func roomMenuBtn(sender: AnyObject) {
        let isHidden = roomOverlayView.hidden
        
        hideOverlays()
        
        var delay : Double
        
        if (!settingsOverlayView.hidden) //if the other view is not hidden. allow a delay of 0.2 sec for the new view to open because the other view needs 0.2 secs to hide
        {
            delay = 0.2
        }
        else
        {
            delay = 0.0
        }
        
        runAfterDelay(delay)
        {
            self.toggleViewFor(self.roomOverlayView, isHidden: isHidden)
        }
    }
    
    @IBAction func settingsMenuBtn(sender: AnyObject) {
        let isHidden = settingsOverlayView.hidden
        
        hideOverlays()
        
        var delay : Double
        
        if (!roomOverlayView.hidden)
        {
            delay = 0.2
        }
        else
        {
            delay = 0.0
        }
        
        runAfterDelay(delay)
            {
            self.toggleViewFor(self.settingsOverlayView, isHidden: isHidden)
        }
    }
    
    func handleTapOnOverlayCanvas(sender: UITapGestureRecognizer? = nil) {
        hideOverlays()
    }
    
    
    
    @IBAction func chartBtnAction(sender: AnyObject) {
        print("chartBtnAction fired")
        
        performSegueWithIdentifier("navigateToChartView", sender: self)
    }
    
    let timelapseImage = UIImage(named: "Timelapse")
    let heatmapImage = UIImage(named: "Map")
    
    @IBAction func timelapseBtnAction(sender: AnyObject) {
        if(timelapseControlsView.hidden)
        {
            legendaView.hidden = true
            timelapseControlsView.hidden = false
            timelapseBtnOutlet.setImage(heatmapImage, forState: .Normal)
        }
        else
        {
            hideTimelapseControls()
        }
    }
    
    var sensorViewControllerInstance : SensorViewController?
    
    var mainStoryboard = UIStoryboard.init(name: "Main", bundle:nil)
    
    var settingsOverlayView = UIView()
    var roomOverlayView = UIView()
    var overlayBounds = CGRect()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        
        NetworkHandler.networkInstance.delegateNTContentViewController = self

        //self.view.sendSubviewToBack(heatmapImageView)
        timelapseControlsView.hidden = true

        
        //overlays are being loaded hidden. use its return value to unhide them in the following fashion: returnedUIView.hidden = false
        //loading settingsOverlayView
        settingsOverlayView = loadViewControllerWithIdentifierToCanvas("SettingsOverlay", canvas: overlayCanvas)
        //loading roomOverlayView
        roomOverlayView = loadViewControllerWithIdentifierToCanvas("RoomListOverlay", canvas: overlayCanvas)
        DataHolder.roomListViewController!.delegate = self;
        
    
        
        //first load the image
        // code to load the image
        
        //init the nodeCanvas
        initNodeCanvas()
        
        // Disable buttons before loading nodes
        chartBtnOutlet.enabled = false
        timelapseBtnOutlet.enabled = false
        

        //non dynamic way of selecting the activeRoom. just for developer purposes.
//        activeRoom = Location(name: "136", siteLocationID: 136, mapPath: "http://.", width: 800, height: 600, descriptionLocation: "Room 136")
        
        //activityIndicator.
        
        if (!isAddSensorToChart){
            UIHelper.applyCornerRadius(activityIndicator, topView: false)
            UIHelper.addLabelToView(activityIndicator, text: "Loading", point: CGPoint(x: activityIndicator.bounds.width * 4, y: 100))
        } else {
            heatmapImageView.image = DataHolder.StaticImageView
            contentViewControllerReady()
        }
        
    }
    
    func contentViewControllerLoading()
    {
        self.activityIndicator.startAnimating()
        self.chartBtnOutlet.enabled = false
        self.timelapseBtnOutlet.enabled = false
    }
    
    func contentViewControllerReady()
    {
        self.activityIndicator.stopAnimating()
        self.chartBtnOutlet.enabled = true
        self.timelapseBtnOutlet.enabled = true
    }
    
    func hideTimelapseControls()
    {
        legendaView.hidden = false
        timelapseControlsView.hidden = true
        disableTimelapse()
        timelapseBtnOutlet.setImage(timelapseImage, forState: .Normal)
    }
    
    func disableTimelapse()
    {
        timeSliderOutlet.value = timeSliderOutlet.maximumValue // first move the slider all the way to the max
        changeTimeSliderValue() // then update everything that should be updated
    }
    
    override func viewWillAppear(animated: Bool) {
        checkDefaultvalues()
    }

    func checkDefaultvalues()
    {
        if  (!NetworkHandler.networkInstance.hasDefaultRoomId())
        {
            performSegueWithIdentifier("navigateToDefaultSettingsFromContentView", sender: nil )
        }
        
        if (DataHolder.StaticActiveRoom == nil)
        {
            NetworkHandler.networkInstance.getActiveRoomForContentViewController()
        } else if (DataHolder.StaticActiveRoom?.uuID != defaults.integerForKey("DefaultRoomId"))
        {
            NetworkHandler.networkInstance.getActiveRoomForContentViewController()
        } else if (DataHolder.StaticActiveRoom?.sensors?.count == 0)
        {
            NetworkHandler.networkInstance.getSensorsByActiveRoomForContentViewController(DataHolder.StaticActiveRoom!)
        }
        
        if (DataHolder.StaticImageView == nil && DataHolder.StaticActiveRoom != nil)
        {
            if let _ = DataHolder.StaticActiveRoom?.mapPath
            {
               NetworkHandler.networkInstance.loadHeatmapImage(DataHolder.StaticActiveRoom!)
            }
        }
    }
    
    func activeRoomReceived (room: Location)
    {
        DataHolder.StaticActiveRoom = room
        NetworkHandler.networkInstance.getSensorsByActiveRoomForContentViewController(room)
        NetworkHandler.networkInstance.loadHeatmapImage(room)
        
        //set roombutton outlet name to match with selected room
        roomBtnOutlet.setTitle(room.name, forState: .Normal)
    }
    
    func recievedHeatMapImage(heatmapImage: UIImage)
    {
        heatmapImageView.image = heatmapImage
        DataHolder.StaticImageView = heatmapImage
    }
    
    func activeRoomSensorListReceivedInActiveRoom (room: Location)
    {
        DataHolder.StaticActiveRoom = room
        NetworkHandler.networkInstance.getMeasurementsBySensorsByActiveRoomForContentViewController(room)
    }

    func last48HrMeasermentsReceivedInActiveRoom (room: Location)
    {
        DataHolder.StaticActiveRoom = room
        
        sensorViewControllerInstance?.activeRoom = room
        sensorViewControllerInstance?.dateToDisplay = NSDate(timeIntervalSince1970: dateTimeOnCreation.timeIntervalSince1970 - 10)
        sensorViewControllerInstance?.drawSensorMeasurements()
        
        runAfterDelay(4) {
            self.activityIndicator.stopAnimating()
            self.chartBtnOutlet.enabled = true
            self.timelapseBtnOutlet.enabled = true
        }
    }
    
    func updateSelectedTime()
    {
        timeSliderOutlet.value += (0.002 * 48) / 30 // step time of: (48 hour /10) / ( 48 * 30 ) because we don't want it to run for 48 seconds but for 30
        
        if (playPauzeBtnOutlet.imageForState(.Normal) == pauseImage)
        {
            if(timeSliderOutlet.value < 0)
            {
                //this repeats this method while the slidervalue is below 0
                self.performSelector("updateSelectedTime", withObject: nil, afterDelay: 0.1)
            }
            else
            {
                // when the timeslideroutlet value is not below 0. its 1 and the playpauzebutton should display the playImage
                playPauzeBtnOutlet.setImage(playImage, forState: .Normal)
            }
        }
        else //in this case the image is not the pauseImage thus it is the playImage. this means the timelapse is running. a click on the playPauzeButton should stop the animation
        {
            NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: "updateSelectedTime", object: nil)
        }
        changeTimeSliderValue()
    }
    
    // UIGestureRecognizerDelegate method
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        if touch.view != nil && touch.view!.accessibilityIdentifier != "overlay" {
            return false
        }
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        print("viewDidAppear")
        if(overlayBounds.height == 0)
        {
            //store correct overlayBounds for later use
            overlayBounds = overlayCanvas.bounds
        }
        //hide the overlayCanvas so it does not obstruct buttons on other views of the page
        overlayCanvas.hidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: Selector("handleTapOnOverlayCanvas:"))
        tap.delegate = self
        overlayCanvas.addGestureRecognizer(tap)
        
        //setting the timelabel indicator to the current date for its label not to say "Button"
        setTimeIndicatorLabel(NSDate())
        if (isAddSensorToChart){
            sensorViewControllerInstance?.activeRoom = DataHolder.StaticActiveRoom!
            sensorViewControllerInstance?.drawSensorMeasurements()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        hideOverlays()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initNodeCanvas()
    {
        //resize the nodecanvas to make its bounds match the heatmapImage bounds.
        nodeCanvas.bounds = heatmapImageView.bounds
        
        sensorViewControllerInstance = mainStoryboard.instantiateViewControllerWithIdentifier("SensorView") as? SensorViewController
        sensorViewControllerInstance?.isAddSensorToChart = isAddSensorToChart
        self.addChildViewController(sensorViewControllerInstance!)
        sensorViewControllerInstance!.didMoveToParentViewController(self)
        sensorViewControllerInstance!.view.frame = nodeCanvas.bounds
        
        
        //sensorViewControllerInstance.delegate = self
        
        
        self.nodeCanvas.addSubview(sensorViewControllerInstance!.view)
    }
    
    func displayNodes()
    {
        //get the nodes in this room and drawNodes with their latest value
    }
    
    func animateNodes()
    {
        //get the nodes in this room and draw the nodes with the value matching the timestamp of the time controls.
    }
    
    func runAfterDelay(delay: NSTimeInterval, block: dispatch_block_t) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), block)
    }
    
    func toggleViewFor(overlayView: UIView, isHidden: Bool)
    {
        if(isHidden)
        {
            // First: set the overlays alpha to 0
            overlayCanvas.alpha = 0
            overlayView.alpha = 0
            
            // Second: display the overlays
            overlayCanvas.hidden = false
            overlayView.hidden = false
            
            // Third: animate the overlays alpha to 1
            UIView.animateWithDuration(0.2, animations: {
                self.overlayCanvas.alpha = 1.0
                overlayView.alpha = 1.0
            })
            
        }
        else
        {
            // First: animate the overlays alpha to 0
            UIView.animateWithDuration(0.2, animations: {
                self.overlayCanvas.alpha = 0
                overlayView.alpha = 0
            })
            
            // Second: wait for the animation to finish, then hide the overlays
            runAfterDelay(0.2) {
                self.overlayCanvas.hidden = true
                overlayView.hidden = true
            }
        }
        runAfterDelay(1.0)
            {
        print("overlayCanvas, overlayView, settingsOverlay,: hidden? and alpha")
        print("\(self.overlayCanvas.hidden) : \(self.overlayCanvas.alpha) :: \(self.roomOverlayView.hidden) : \(self.roomOverlayView.alpha) :: \(self.settingsOverlayView.hidden) : \(self.settingsOverlayView.alpha)")
        }
    }
    
    func hideOverlays()
    {
        if(!settingsOverlayView.hidden)
        {
            toggleViewFor(settingsOverlayView, isHidden: false)
        }
        
        if(!roomOverlayView.hidden)
        {
            toggleViewFor(roomOverlayView, isHidden: false)
        }
        
    }
    
    func loadViewControllerWithIdentifierToCanvas(viewControllerIdentifier: String, canvas: UIView) -> UIView{
        let sourceView = mainStoryboard.instantiateViewControllerWithIdentifier(viewControllerIdentifier)
        self.addChildViewController(sourceView)
        sourceView.didMoveToParentViewController(self)
        sourceView.view.frame = canvas.bounds
        
        //sensorViewControllerInstance.delegate = self
        
        canvas.addSubview(sourceView.view)
        
        let returnView = sourceView.view
        //the added sourceView's view gets hidden = true to hide it.
        returnView.hidden = true
        
        return returnView
    }
    
    func roomSelected(location: Location) {
        print("roomFromContentViewController \(location.name)")
        self.defaults.setInteger(location.uuID, forKey: "DefaultRoomId")
        roomBtnOutlet.setTitle(location.name, forState: .Normal)
        sensorViewControllerInstance?.removeCurrentSensorSubviews()
        
        hideOverlays()
        checkDefaultvalues()
        
        hideTimelapseControls()
        contentViewControllerLoading()
    }
}