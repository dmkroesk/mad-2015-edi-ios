//
//  NotificationSettingsViewController.swift
//  EEMON
//
//  Created by Mitchell Slager on 13/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class NotificationSettingsViewController: UIViewController {
    // User defaults
    var defaults : NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    @IBOutlet var superView: UIView!

    // Switch
    @IBOutlet weak var notifySwitch: UISwitch!
    
    // Slider
    @IBOutlet weak var wSlider: UISlider!
    @IBOutlet weak var wSliderValue: UILabel!
    
    @IBAction func notifySwitchClick(sender: AnyObject) {
        if notifySwitch.on {
            defaults.setBool(true, forKey: "SwitchState")
            wSlider.enabled = true
        } else {
            defaults.setBool(false, forKey: "SwitchState")
            wSlider.enabled = false
        }
    }
    
    @IBAction func wSliderValueChanged(sender: UISlider) {
        let selectedValue = Int(sender.value)
        
        wSliderValue.text = String(stringInterpolationSegment: selectedValue) + "W"
        
        defaults.setFloat(sender.value, forKey: "SliderValue")
    }
    
    func sendNotification() {
        let localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "Sensor is using # Watt"
        localNotification.alertBody = "Sensor # has a Wattage value of #."
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 1)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Disable slider on load
        wSlider.enabled = false
        
        // Check & set Switch state
        if (defaults.objectForKey("SwitchState") != nil) {
            notifySwitch.on = defaults.boolForKey("SwitchState")
        }
        
        // Check & set Slider value
        if (defaults.objectForKey("SliderValue") != nil) {
            wSlider.value = defaults.floatForKey("SliderValue")
            wSlider.enabled = defaults.boolForKey("SwitchState")
        }

        // Do any additional setup after loading the view.
        for sv in superView.subviews {
            sv.layer.cornerRadius = 4
            sv.clipsToBounds = true
        }
        
        // Set slider value label to the current slider value on load
        wSliderValue.text = "\(Int(wSlider.value))" + "W"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
