//
//  RoomListViewController.swift
//  EEMON
//
//  Created by Mitchell Slager on 12/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit
import Foundation

protocol RoomSelectedDelegate
{
    func roomSelected(location : Location)
}

class RoomListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, doneNTRoomListViewController {

    var delegate : RoomSelectedDelegate?
    
    var rooms : [Location] = [Location]()
    
    
    @IBOutlet weak var tableViewOutlet: UITableView!
    @IBOutlet weak var tableViewWrapper: UIView!
    @IBOutlet weak var scrollViewOutlet: UIScrollView!
    @IBOutlet weak var overlay: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewOutlet.delegate = self
        tableViewOutlet.dataSource = self
        NetworkHandler.networkInstance.delegateNTRoomListViewController = self
        DataHolder.roomListViewController = self
        
        overlay.accessibilityIdentifier = "overlay"
        
        
        for(var i = 1; i < 40; i++)
        {
            scrollViewOutlet.addSubview(getViewWithLabel("test", x: 10, y: 20 * CGFloat(i) ))
        }
        
        print("scrollviewoutletbounds: \(scrollViewOutlet.bounds.origin.x) : \(scrollViewOutlet.bounds.origin.y) WH: \(scrollViewOutlet.bounds.width) : \(scrollViewOutlet.bounds.height)")

    }
    
    func getViewWithLabel(text : String, x : CGFloat, y : CGFloat) ->UIView
    {
        let label = UILabel(frame: CGRectMake(x, y, 250, 40))
        label.text = text
        
        return label
    }
    
    override func viewWillAppear(animated: Bool) {
        NetworkHandler.networkInstance.getRoomsForRoomListViewController()

    }
    
    override func viewDidAppear(animated: Bool) {
        UIHelper.applyCornerRadius(tableViewWrapper, topView: true)
        UIHelper.applyPlainShadow(tableViewWrapper)

    }

    func roomsReceived (rooms: [Location])
    {
        self.rooms = rooms
        tableViewOutlet.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title: UILabel = UILabel()
        let bottomLine = CALayer()
        let width = CGFloat(2.0)
        
        // Define bottom line
        bottomLine.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.12).CGColor
        bottomLine.frame = CGRect(x: 0, y: 28, width: view.frame.size.width, height: 2)
        bottomLine.borderWidth = width
        
        // Define title
        title.text = "ROOMS"
        title.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.83)
        title.font = UIFont.boldSystemFontOfSize(13)
        title.backgroundColor = UIColor.whiteColor()
        title.textAlignment = .Center
        
        // Add bottom line to title
        title.layer.addSublayer(bottomLine)
        title.layer.masksToBounds = true
        
        return title
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("roomCell", forIndexPath: indexPath) as! RoomListTableViewCell

        cell.roomIdentifier.text = rooms[indexPath.row].name
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableViewOutlet.deselectRowAtIndexPath(indexPath, animated: true)
        
        print(rooms[indexPath.row].name)
        if(nil != delegate)
        {
            delegate?.roomSelected(rooms[indexPath.row])
        }
    }
}
