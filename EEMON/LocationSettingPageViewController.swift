//
//  LocationSettingPageViewController.swift
//  EEMON
//
//  Created by Pascal Huijsmans on 15/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class LocationSettingPageViewController: UIPageViewController {

    private let vcIdentiefiers : NSArray = ["LocationSettingViewController_A","LocationSettingViewController_B","LocationSettingViewController_C"]
    private let vcStartupIdentifier = "LocationSettingViewController_A"

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier(vcStartupIdentifier)
        
        setViewControllers([vc], direction: .Forward, animated: false, completion: nil)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
