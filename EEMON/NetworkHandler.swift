//
//  NetworkHandler.swift
//  EEMON
//
//  Created by Pascal Huijsmans on 22/10/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SystemConfiguration


public class NetworkHandler : NSObject  {
    
    static let networkInstance = NetworkHandler()

    let ediUsername : String
    let ediPassword : String
    let baseUrl : String
    let apiPath : String
    let imagePath : String
    let defaults: NSUserDefaults
    
    // temp values
    var count = 0
    var tempRoom: Location?
    var tempSensors:[Sensor] = [Sensor]()
    var timer = NSTimer()
    
    // NT staat voor Network Tasks
    var delegateNTContentViewController:doneNTContentViewController?
    var delegateNTOrganisationTableViewController:doneNTOrganisationTableViewController?
    var delegateNTBuildingTableViewController:doneNTBuildingTableViewController?
    var delegateNTRoomTableViewController:doneNTRoomTableViewController?
    var delegateNTRoomListViewController:doneNTRoomListViewController?
    
    override init(){
        self.ediUsername = "ediuser"
        self.ediPassword = "edipassword"
        self.apiPath = "/api"
        self.imagePath = "image"
        self.baseUrl = "http://145.48.6.95:8080"
        self.defaults = NSUserDefaults.standardUserDefaults()
    }

    // **---- begin: network establish functions  ----** //
    
    // checking internet connection
    func checkConnectionWithAccessToken() {
        if (isServerReachable())
        {
            if (isAccesTokenValid()){
                if let token = self.defaults.stringForKey("xAccessToken") {
                    let url = baseUrl + apiPath + "/ping"
                    let headers = ["X-Access-Token": token]
                    
                    //Alamofire.request(.GET, url, parameters: parameters, encoding: .JSON, headers: headers)
                    Alamofire.request(.GET, url, headers: headers)
                        .responseJSON{ request, response, data in
                        if (self.isDataFromAlamofireValid(data.value)){
                            let json = JSON (data.value!)
                            
                            if(json["status"].intValue == 401 || json["status"].intValue == 404)
                            {
                                print (json["message"].string)
                                self.showMessageAlert("json error message internet connection check")
                            }
                            else
                            {
                                print("Network connection established")
                            }
                        }
                        else{
                            self.showMessageAlert("no valid data internet connection check")
                        }
                    }
                }
            }
            else {
                self.getAccessToken();
            }
        }
        else
        {
            showMessageAlert("server is not reachable")
        }
    }
    
    // checking is token is valid
    func isAccesTokenValid() -> Bool {
        
        if let xAccessToken : String = self.defaults.stringForKey("xAccessToken")
        {
            if let xAccessTokenExpire : Double = self.defaults.doubleForKey("xAccessTokenExpire")
            {
                if ((xAccessToken.isEmpty) == false)
                {
                    let date = NSDate()
                    if ((xAccessTokenExpire.isZero) == false){
                        return true
                    }
                    
                    let tokenDate = NSDate(timeIntervalSince1970: xAccessTokenExpire)
                    
                    //Comparing date with tokendate; if the date is earlier then tokendate return true
                    if (date.compare(tokenDate) == NSComparisonResult.OrderedDescending ){
                        return true
                    }
                }
            }
        }
        
        return false
    }
    
    // get access token
    func getAccessToken () {
        let tokenUrl = baseUrl + apiPath + "/login"
        
        let parameters = ["username": ediUsername, "password": ediPassword]
        
        Alamofire.request(.POST, tokenUrl, parameters: parameters, encoding: .JSON)
            .responseJSON{ request, response, data in
            if (self.isDataFromAlamofireValid(data.value)){
                let json = JSON (data.value!)
                
                if(json["status"].intValue == 401 || json["status"].intValue == 404)
                {
                    self.showMessageAlert(json["message"].string!)
                }
                else
                {
                    self.defaults.setObject(json["token"].string, forKey: "xAccessToken")
                    self.defaults.setObject(json["expires"].doubleValue, forKey: "xAccessTokenExpire")

                   self.accesTokenDone()
                }
            }
            else{
                self.showMessageAlert("no data access token")
            }
        }
    }
    
    func isServerReachable()->Bool{
        
        var Status:Bool = false
        let url = NSURL(string: "\(baseUrl)")
        let request = NSMutableURLRequest(URL: url!)
        request.HTTPMethod = "HEAD"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        
        var response: NSURLResponse?

        do {
            _ = try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        } catch (_) {
           // print(e)
        }
        
        if let httpResponse = response as? NSHTTPURLResponse {
            let httpResponseCode: Int = httpResponse.statusCode
            if httpResponseCode == 200 {
                Status = true
            }
        }
        return Status
    }
    
    // afhandeling netwerk problemen met connectie
    func showMessageAlert(message: String){
        let alert = UIAlertView()
        alert.title = "Application Message"
        alert.message = message
        alert.addButtonWithTitle("Ok")
        alert.show()
    }
    
    func isDataFromAlamofireValid(arg : AnyObject?) -> Bool{
        if (arg != nil){
            return true
        }
        else{
            return false
        }
    }
    
    func accesTokenDone ()
    {
        self.checkConnectionWithAccessToken()
    }
    // **---- end: network establish functions  ----** //
    // **---- begin: defaults check functions  ----** //
    
    func hasDefaultOrganisationId() -> Bool
    {
        if (self.defaults.integerForKey("DefaultOrganisationId") != 0)
        {
            return true
        }
        return false
    }
    
    func hasDefaultBuildingId() -> Bool
    {
        if (self.defaults.integerForKey("DefaultBuildingId") != 0)
        {
            return true
        }
        return false
    }
    
    func hasDefaultRoomId() -> Bool
    {
        if (self.defaults.integerForKey("DefaultRoomId") != 0)
        {
            return true
        }
        return false
    }

    // **---- end: defaults check functions  ----** //
    // **---- begin: data functions contentview controller  ----** //

    func getActiveRoomForContentViewController()
    {
        self.checkConnectionWithAccessToken()
        
        if (hasDefaultRoomId())
        {
            let defaultRoomIdKey = self.defaults.integerForKey("DefaultRoomId").description
            let url = baseUrl + apiPath + "/rooms/" + defaultRoomIdKey
            
            if let token = self.defaults.stringForKey("xAccessToken")
            {
                let headers = ["X-Access-Token": token]
                
                Alamofire.request(.GET, url, headers: headers)
                    .responseJSON{ request, response, data in
                if (self.isDataFromAlamofireValid(data.value)){
                    let json = JSON (data.value!)
                    
                    var room : Location
                    
                    if let items = json["room"].array
                    {
                        for item in items
                        {
                            let id = item["ID"].intValue
                            let name = item["Name"].description
                            let description = item["Description"].description
                            let mapPath = item["MapPath"].description
                            let width = item["Width"].floatValue
                            let height = item["Height"].floatValue
                            let buildingID = item["BuildingID"].intValue
                            
                            room = Location(uuid: id, name: name, mapPath: mapPath, width: width, height: height, descriptionLocation: description, buildingID: buildingID)
                            self.delegateNTContentViewController?.activeRoomReceived(room)
                        }
                    }
                  }
               }
            }
        }
    }

    func getSensorsByActiveRoomForContentViewController(room: Location)
    {
        self.checkConnectionWithAccessToken()
        
        if (hasDefaultRoomId())
        {
            let defaultRoomIdKey = self.defaults.integerForKey("DefaultRoomId").description
            let url = baseUrl + apiPath + "/rooms/" + defaultRoomIdKey + "/sensors"
            
            if let token = self.defaults.stringForKey("xAccessToken")
            {
                let headers = ["X-Access-Token": token]
                
                Alamofire.request(.GET, url, headers: headers)
                    .responseJSON{ request, response, data in
                    if (self.isDataFromAlamofireValid(data.value)){
                        let json = JSON (data.value!)
                        
                        var sensors : [Sensor] = []
                        
                        if let items = json["sensors"].array
                        {
                            for item in items
                            {
                                let uuid = item["ID"].intValue
                                let name = item["Name"].description
                                let description = item["Description"].description
                                let nodeid = item["NodeID"].intValue
                                let deviceIdentifier = item["Deviceidentifier"].description
                                let type = item["Type"].description
                                let mapLocationX = item["MapLocationX"].floatValue
                                let mapLocationY = item["MapLocationY"].floatValue
                                
                                let sensor = Sensor(uuid: uuid, name: name, nodeID: nodeid, deviceIdentifier: deviceIdentifier, type: type, descriptionSensor: description, mapLocationX: mapLocationX, mapLocationY: mapLocationY, location : room)
                                
                                sensors.append(sensor)
                            }
                            room.sensors = sensors
                            self.delegateNTContentViewController?.activeRoomSensorListReceivedInActiveRoom(room)
                        }
                    }
                }
            }
        }
    }

    func getMeasurementsBySensorsByActiveRoomForContentViewController(room: Location)
    {
        if (hasDefaultRoomId())
        {
            let sensors : [Sensor] = room.sensors!
            
            if (!sensors.isEmpty)
            {
                self.count = 0
                self.tempSensors = sensors
                self.tempRoom = room
                
                getMeasurementsForCurrentRoom()
            }
            else
            {
                showMessageAlert("Room " + room.name + " doesn't have any sensors to view.")
            }
        }
    }
    
    func measurementsRecieved()
    {
        print("measurementsReceived")
        //timer = NSTimer.scheduledTimerWithTimeInterval(2.0,
         //   target: self, selector: "handleMeasurmentRequest", userInfo: nil, repeats: false)
        
                self.tempRoom?.sensors = self.tempSensors
                self.delegateNTContentViewController?.last48HrMeasermentsReceivedInActiveRoom(self.tempRoom!)
    }
    
    func getMeasurementsForCurrentRoom()
    {
        for (self.count = 0; self.count < tempSensors.count; self.count++)
        {
            if let token = self.defaults.stringForKey("xAccessToken")
            {
                let uuid = self.tempSensors[self.count].uuID
                let url = baseUrl + apiPath + "/sensors/" + uuid.description + "/measurements/48"
                
                let headers = ["X-Access-Token": token]
                print("request made with count: \(self.count)")
                
                Alamofire.request(.GET, url, headers: headers)
                    .responseJSON{ request, response, data in
                        if (self.isDataFromAlamofireValid(data.value)){
                            let json = JSON (data.value!)
                            
                            if let items = json["measurements"].array
                            {
                                var measurements = [Measurement]()
                                for item in items
                                {
                                    let timestamp = item["Timestamp"].doubleValue
                                    let sensorId = item["SensorID"].intValue
                                    let value = item["Value"].doubleValue
                                    let measurementUnit = item["MeasurementUnit"].description
                                    
                                    let date = NSDate(timeIntervalSince1970: timestamp)
                                    
                                    let measurement = Measurement (sensorID: sensorId, timeStamp: date, unit: measurementUnit, value: value )
                                    
                                    measurements.append(measurement)
                                }
                                

                                for s in self.tempSensors
                                {
                                    print("s.uuID and measurement sensorID: \(s.uuID) : \(measurements.last?.sensorID)")
                                    if(s.uuID == measurements.last?.sensorID)
                                    {
                                        s.measurements = measurements
                                        print("request handled for sensorID: \(s.uuID)")
                                    }
                                }
  
                            }

                            
                            if (self.count == self.tempSensors.count)
                            {
                                self.measurementsRecieved()
                            }
                        }
                }
            }
        }
    }

    
    func loadHeatmapImage(room: Location)
    {
        let url = room.mapPath
        
        if (!url.isEmpty)
        {
            Alamofire.request(.GET, url).response(){
                (_, _, data, _) in
                let heatmapImage = UIImage(data: data! as NSData)
                self.delegateNTContentViewController?.recievedHeatMapImage(heatmapImage!)
            }
        }
        else
        {
            showMessageAlert("No image url link found in the selected room")
        }
    }

    // **---- end: data functions contentview controller  ----** //
    // **---- begin: data functions organisation tableview controller  ----** //
    
    func getOrganisationsForOrganisationTableViewController ()
    {
        self.checkConnectionWithAccessToken()
        
        let url = baseUrl + apiPath + "/organisations"
        
        if let token = self.defaults.stringForKey("xAccessToken")
        {
            let headers = ["X-Access-Token": token]

            Alamofire.request(.GET, url, headers: headers)
                .responseJSON{ request, response, data in
                if (self.isDataFromAlamofireValid(data.value)){
                    let json = JSON (data.value!)

                    var organisations: [Organisation] = []
                    
                    if let items = json["organisations"].array
                    {
                        for item in items
                        {
                            let id = item["ID"].intValue
                            let name = item["Name"].description
                            let description = item["Description"].description
                            
                            organisations.append(Organisation(uuid: id, name: name, description: description))
                        }
                    }
                    self.delegateNTOrganisationTableViewController?.organisationsReceived(organisations)
                }
            }
        }
    }
    
    // **---- end: data functions organisation tableview controller  ----** //
    // **---- begin: data functions building tableview controller  ----** //
    
    func getBuildingsForBuildingTableViewController (organisation: Organisation)
    {
        self.checkConnectionWithAccessToken()
        
        let url = baseUrl + apiPath + "/organisations/\(String(organisation.uuID))/buildings"
        
        if let token = self.defaults.stringForKey("xAccessToken")
        {
            let headers = ["X-Access-Token": token]
            
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON{ request, response, data in
                if (self.isDataFromAlamofireValid(data.value)){
                    let json = JSON (data.value!)
                    
                    var buildings: [Building] = []
                    
                    if let items = json["buildings"].array
                    {
                        for item in items
                        {
                            let id = item["ID"].intValue
                            let name = item["Name"].description
                            let description = item["Description"].description
                            let street = item["Street"].description
                            let streetNumber = item["StreetNumber"].description
                            let postalcode = item["Postalcode"].description
                            let city = item["City"].description
                            
                            buildings.append(Building(uuid: id, name: name, street: street, city: city, streetNumber: streetNumber, postalCode: postalcode, descriptionBuilding: description))
                        }
                    }
                    self.delegateNTBuildingTableViewController?.buildingsReceived(buildings)
                }
            }
        }
    }
    
    // **---- end: data functions building tableview controller  ----** //
    // **---- begin: data functions room tableview controller  ----** //
    
    func getRoomsForRoomTableViewController (building: Building)
    {
        self.checkConnectionWithAccessToken()
        
        let url = baseUrl + apiPath + "/buildings/\(String(building.uuID))/rooms"
        
        if let token = self.defaults.stringForKey("xAccessToken")
        {
            let headers = ["X-Access-Token": token]
            
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON{ request, response, data in
                if (self.isDataFromAlamofireValid(data.value)){
                    let json = JSON (data.value!)
                    
                    var rooms: [Location] = []
                    
                    if let items = json["rooms"].array
                    {
                        for item in items
                        {
                            let id = item["ID"].intValue
                            let name = item["Name"].description
                            let description = item["Description"].description
                            let mapPath = item["MapPath"].description
                            let width = item["Width"].floatValue
                            let height = item["Height"].floatValue
                            let buildingID = item["BuildingID"].intValue

                            rooms.append(Location(uuid: id, name: name, mapPath: mapPath, width: width, height: height, descriptionLocation: description, buildingID: buildingID))
                        }
                    }
                    self.delegateNTRoomTableViewController?.roomsReceived(rooms)
                }
            }
        }
    }
    
    // **---- end: data functions room tableview controller  ----** //
    // **---- begin: data functions room listview controller  ----** //
    
    func getRoomsForRoomListViewController ()
    {
        self.checkConnectionWithAccessToken()
        
        if (hasDefaultBuildingId())
        {
            let defaultBuildingIdKey = self.defaults.integerForKey("DefaultBuildingId").description
            let url = baseUrl + apiPath + "/buildings/\(defaultBuildingIdKey)/rooms"
            
            if let token = self.defaults.stringForKey("xAccessToken")
            {
                let headers = ["X-Access-Token": token]
                
                Alamofire.request(.GET, url, headers: headers)
                    .responseJSON{ request, response, data in
                    if (self.isDataFromAlamofireValid(data.value)){
                        let json = JSON (data.value!)
                        
                        var rooms: [Location] = []
                        
                        if let items = json["rooms"].array
                        {
                            for item in items
                            {
                                let id = item["ID"].intValue
                                let name = item["Name"].description
                                let description = item["Description"].description
                                let mapPath = item["MapPath"].description
                                let width = item["Width"].floatValue
                                let height = item["Height"].floatValue
                                let buildingID = item["BuildingID"].intValue
                                
                                rooms.append(Location(uuid: id, name: name, mapPath: mapPath, width: width, height: height, descriptionLocation: description, buildingID: buildingID))
                            }
                        }
                        self.delegateNTRoomListViewController?.roomsReceived(rooms)
                    }
                }
            }
        }
    }
    
    // **---- end: data functions room listview controller  ----** //
}

// Network Internal Delegate
protocol doneNetworkTasksItern {
    func accesTokenDone ()
    func measurementsRecieved()
}

// Delegate to ContentViewController
protocol doneNTContentViewController {
    func activeRoomReceived (room: Location)
    func activeRoomSensorListReceivedInActiveRoom (room: Location)
    func last48HrMeasermentsReceivedInActiveRoom (room: Location)
    func recievedHeatMapImage(heatmapImage: UIImage)
}

// Delegate to OrganisationTableViewController
protocol doneNTOrganisationTableViewController {
    func organisationsReceived (organisations: [Organisation])
}

// Delegate to BuildingTableViewController
protocol doneNTBuildingTableViewController {
    func buildingsReceived (buildings: [Building])
}

// Delegate to RoomsTableViewController
protocol doneNTRoomTableViewController {
    func roomsReceived (rooms: [Location])
}

// Delegate to RoomsListViewController
protocol doneNTRoomListViewController {
    func roomsReceived (rooms: [Location])
}