//
//  SensorViewController.swift
//  EEMON
//
//  Created by Jack Evers on 04/11/15.
//  Copyright © 2015 Mad team eemon. All rights reserved.
//

import UIKit

class SensorViewController: UIViewController, UIGestureRecognizerDelegate{
    
    @IBOutlet weak var nodeCanvas: UIView!
    @IBOutlet weak var activeCanvas: UIView!
    
    var activeRoom : Location?
    
    var activeDialogView : UIView?
    var dateToDisplay : NSDate?
    
    var activeSensor : Sensor?
    var isAddSensorToChart = false

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        activeCanvas.hidden = true
        
        
        drawSensorMeasurements()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawSensorMeasurements()
    {
        if(nil == dateToDisplay)
        {
            dateToDisplay = NSDate()
        }
            
        removeCurrentSensorSubviews()

        if(nil != activeRoom)
        {
            for s in activeRoom!.sensors!
            {
                let sensor = s
                sensor.draw(nodeCanvas, onDateTime: dateToDisplay!)
                //print("sensor name and value and timestamp: " + sensor.name + " : " + sensor.getMeasurementClosestTo(dateToDisplay!).description + " dateToDisplay: " + dateToDisplay!.timeIntervalSince1970.description )
            }
        }
    }
    
    func removeSubviewWithIdentifier(str : String)
    {
        for sv in nodeCanvas.subviews
        {
            if(sv.accessibilityIdentifier == str)
            {
                sv.removeFromSuperview()
            }
        }
    }
    
    func removeCurrentSensorSubviews()
    {
        //TODO we might want to check which sensorviews should NOT be drawn again and only draw the ones that need to be drawn
        //instead of clearing the entire canvas and then redrawing all the sensors
        removeSubviewWithIdentifier("sensorView")
    }
    
    func addTestSensor(x : Float, y : Float, w : Double, name: String, location: Location)
    {
//        let s = Sensor(uuid: 1, name: name, nodeID: 1, deviceIdentifier: "ddd", type: "fibarPlug", descriptionSensor: "Far left corner workplaces", mapLocationX: x, mapLocationY: y, location: activeRoom)
//        
//        s.setActionTrgt(self)
//        
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate() - 170000, unit: "Watt", value: 100.0))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate() - 160000, unit: "Watt", value: 500.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 150000, unit: "Watt", value: 800.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 140000, unit: "Watt", value: 290.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 120000, unit: "Watt", value: 610.5))
//        s.measurements.append(Measurement(sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 100000, unit: "Watt", value: 2200.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 80000, unit: "Watt", value: 180.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 60000, unit: "Watt", value: 590.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 40000, unit: "Watt", value: 1010.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970 - 20000, unit: "Watt", value: 2200.5))
//        s.measurements.append(Measurement( sensorID: 2, timeStamp: NSDate().timeIntervalSince1970, unit: "Watt", value: w))
        
        //activeRoom.sensors = [Sensor]()
      //  activeRoom.sensors?.append(s)

        //drawSensorMeasurements()
    }
    
    func pressed(sender:UINodeControl!){
        print("sensor tapped")
        //addTestSensor(Float(activeRoom.sensorList.count * 10),y: 200, w: Double(activeRoom.sensorList.count * 300))
        drawOrHideSensorDialog(sender)
        drawSensorMeasurements()
    }
    
    func drawOrHideSensorDialog(sender: UINodeControl!)
    {
        if(nil != activeDialogView)
        {
            activeDialogView = nil
            activeCanvas.hidden = true
            clearActiveCanvas()
            removeBackground()
            removeSubviewWithIdentifier("sensorDialog")
        }
        else
        {
            activeCanvas.hidden = false
            UIHelper.displayBackground(nodeCanvas, view: self, action: Selector("handleTapOnBackground:"))
            drawSensorDialog(sender.sensor!, isAddSensor:  isAddSensorToChart)
            sortSubViews(sender)
            
        } 
    }
    
    func clearActiveCanvas()
    {
        for sv in activeCanvas.subviews
        {
            sv.removeFromSuperview()
        }
    }
    
    func sortSubViews(svClicked : UINodeControl)
    {        
        for sv in nodeCanvas.subviews
        {
            if (sv.accessibilityIdentifier == "background")
            {
                activeCanvas.addSubview(sv)
            }
            if (sv.accessibilityIdentifier == "sensorDialog")
            {
                activeCanvas.addSubview(sv)
            }
        }
        activeCanvas.addSubview(svClicked)
    }
    
    func drawSensorDialog(sensor : Sensor, isAddSensor : Bool)
    {
        activeSensor = sensor
        
        let scaleX = Float(nodeCanvas.bounds.width) / sensor.location.width
        let scaleY = Float(nodeCanvas.bounds.height) / sensor.location.height
        var x = CGFloat(sensor.mapLocationX * scaleX)
        var y = CGFloat(sensor.mapLocationY * scaleY)
        
        if(x > 750 && y < 100)
        {
            x = 750
            y += 190
        }
        else
        {
            if(x > 750)
            {
                x = 750
                y -= 20
            }
            
            if(y < 100)
            {
                y = 100
                x += 20
            }
        }
        
        let testFrame : CGRect = CGRectMake(x + sensor.heatDiameter ,y - 80 ,150 ,90)
        let view : UIView = UIView(frame: testFrame)
        
        view.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //white background with 0 alpha
        
        addLabelToView(view, text:  sensor.name, point: CGPointMake(85, 20))
        var valToDisplay = sensor.getMeasurementClosestTo(dateToDisplay!).description
        if(valToDisplay.containsString("-1"))
        {
            valToDisplay = "N/A"
        }
        else
        {
            valToDisplay += " W"
        }
        addLabelToView(view, text: valToDisplay, point: CGPointMake(85, 40))
        addButtonToView(view, text: (isAddSensor) ? "Add Sensor" : "View Chart",target: self , point: CGPointMake(50, 70))
        UIHelper.applyCornerRadius(view, topView: false)
        UIHelper.applyPlainShadow(view)
        
        view.accessibilityIdentifier = "sensorDialog"
        
        activeDialogView = view
        nodeCanvas.addSubview(view)
    }
    
    func removeBackground()
    {
        removeSubviewWithIdentifier("background")
    }
    
    func addLabelToView(view: UIView, text : String, point : CGPoint){
        let label = UILabel(frame: view.bounds)
        label.center = point
        label.textAlignment = NSTextAlignment.Left
        label.text = text
        view.addSubview(label)
    }
    
    func addButtonToView(view: UIView, text : String,target : AnyObject, point : CGPoint){
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.frame = view.bounds
        button.center = point
        button.setTitle(text, forState: UIControlState.Normal)
        button.addTarget(target, action: "chartButtonTapped:", forControlEvents: UIControlEvents.TouchUpInside)
        button.enabled = (activeSensor?.measurements.count > 0 ? true : false)
        view.addSubview(button)
    }
    
    func chartButtonTapped(sender:UIButton)
    {
        if(nil != activeSensor)
        {
            if (isAddSensorToChart){
                DataHolder.addSensorPopDelegate?.didAddSensorPop(activeSensor!)
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
            }
            else {
                performSegueWithIdentifier("navigateToSingleChartView", sender: activeSensor)
            }
        }
    }
    
    func handleTapOnBackground(sender: UITapGestureRecognizer? = nil) {
        drawOrHideSensorDialog(nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "navigateToSingleChartView" {
            if let destination = segue.destinationViewController as? SingleChartViewController {
                let segueSensor = self.activeSensor
                    
                destination.sensors?.append(segueSensor!)
            }
        }
    }
}

